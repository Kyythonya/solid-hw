﻿using System;

namespace ISP
{
    public interface ICoder
    {
        void Code();
    }

    public interface ITester
    {
        void WriteTest();
    }

    public class Programmer : ICoder, ITester
    {
        public void Code()
        {
            Console.WriteLine("\tCoding");
        }

        public void WriteTest()
        {
            Console.WriteLine("\tWriting unit tests");
        }
    }

    public class Tester : ITester
    {
        public void WriteTest()
        {
            Console.WriteLine("\tWriting system tests");
        }
    }

    public class Program
    {
        static void Main(string[] args)
        {
            var programmer = new Programmer();

            Console.WriteLine("Programmer can do:");
            programmer.Code();
            programmer.WriteTest();

            var tester = new Tester();

            Console.WriteLine("\nExcept for a tester, he can do:");
            tester.WriteTest();

            Console.ReadLine();
        }
    }
}
